# Lufi's authors

## Main developers

* Luc Didry, aka Sky (<https://www.fiat-tux.fr>), core developer, @framasky on [Twitter](https://twitter.com/framasky) and on [Diaspora*](https://framasphere.org/public/framasky)

## Contributors

* Nikos Filopoulos (italian translation)
* Framartin (fix french translation)
* Sébastien Duthil (fix english translation)
* Armando Lüscher, https://noplanman.ch/ (fix css)
* Quentin Pagès (occitan translation)
* Yann Le Brech (htpasswd file support)
* Jéssica Da Cunha (portuguese translation)
* Ilker Kulgu (fix IE11 compatibility, Dutch translation)
* Stéphane Baron (bugfix)
* Butterfly of Fire (arabic translation)
* Frju365 (german translation)

## Vulnerabilities / bug hunters

Lufi participated to a [Hackpéro](https://hackpero.com/) (sort of a bug bounty hackathon), thanks to [Bounty factory](https://hackpero.com/).

Many thanks to those who found bugs and vulnerabilities:

* joker2a
* March
* Nicknam3
* SaxX
* tfairane
